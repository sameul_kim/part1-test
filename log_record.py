import logging
from pythonjsonlogger import jsonlogger


class NewLogRecord(logging.LogRecord):
    @property
    def ch_var1(self):
        return getattr(self, 'var1', None)


def clickhouse_filter(record: NewLogRecord):
    return record.ch_var1 is not None


logging.setLogRecordFactory(NewLogRecord)

logger = logging.getLogger('simple_example')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.addFilter(clickhouse_filter)
#formatter = logging.Formatter('%(asctime)s - var1: %(var1)s - %(message)s')
formatter = jsonlogger.JsonFormatter()
ch.setFormatter(formatter)
logger.addHandler(ch)

logger.info('test1', )
logger.info('test1', extra={'var1': 123})
